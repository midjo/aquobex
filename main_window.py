# -*- coding: utf-8 -*-
import sys
import random

from PyQt4 import QtGui, QtCore
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg
from matplotlib.figure import Figure


class MatplotlibWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(MatplotlibWidget, self).__init__(parent)
        self.width = 4
        self.height = 2

        self.figure = Figure(figsize=(self.width, self.height), facecolor='#ececec')
        self.canvas = FigureCanvasQTAgg(self.figure)

        self.axis = self.figure.add_subplot(111)
        self.axis.set_facecolor('#ececec')

        self.layoutVertical = QtGui.QVBoxLayout(self)
        self.layoutVertical.addWidget(self.canvas)

        self.plot()

    def plot(self):
        data = [random.random() for i in range(10)]
        self.axis.plot(data, '.-')


class MainWindow(QtGui.QWidget):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.resize(800, 500)
        self.setWindowTitle('Messuring Water Level')
        self.initUI()

    def initUI(self):
        self.matplotlib_widget = MatplotlibWidget(self)

        self.water_heigh = QtGui.QLabel("Water height")
        self.water_heigh_desc = QtGui.QLabel("U mm ili cm")

        self.water_vel = QtGui.QLabel("Water velocity")
        self.water_vel_desc = QtGui.QLabel("14234 mm/s")

        self.men = QtGui.QLabel("Men needed:")
        self.men_desc = QtGui.QLabel("Nekoliko ljudi")

        self.state = QtGui.QLabel("State:")
        self.state_desc = QtGui.QLabel("Alert/Warning/EM")

        self.beacon = QtGui.QLabel("Beacon")
        self.beacon_img = QtGui.QLabel("Ovjde ide neka slika")

        self.siren = QtGui.QLabel("Siren")
        self.siren_img = QtGui.QLabel("Ovjde ide neka slika")

        self.show()

        self.center()
        self.set_layout()

    def center(self):
        qr = self.frameGeometry()
        cp = QtGui.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def set_layout(self):
        left_vertical_layout = QtGui.QVBoxLayout()
        right_vertical_layout = QtGui.QVBoxLayout()

        main_layout = QtGui.QHBoxLayout()
        main_layout.addLayout(left_vertical_layout)
        main_layout.addLayout(right_vertical_layout)

        left_vertical_layout.addWidget(self.matplotlib_widget)

        left_vertical_layout.addWidget(self.water_heigh)
        left_vertical_layout.addWidget(self.water_heigh_desc)

        left_vertical_layout.addWidget(self.water_vel)
        left_vertical_layout.addWidget(self.water_vel_desc)

        right_vertical_layout.addWidget(self.men)
        right_vertical_layout.addWidget(self.men_desc)

        right_vertical_layout.addWidget(self.state)
        right_vertical_layout.addWidget(self.state_desc)

        img_layout = QtGui.QGridLayout()
        right_vertical_layout.addLayout(img_layout)
        img_layout.addWidget(self.beacon, 0, 0)
        img_layout.addWidget(self.beacon_img, 0, 1)
        img_layout.addWidget(self.siren, 1, 0)
        img_layout.addWidget(self.siren_img, )
        self.setLayout(main_layout)


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec_())